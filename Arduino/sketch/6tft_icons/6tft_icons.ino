#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library

// Files with the bitmaps data
#include "bitmaps.h"
//#include "bitmapsLarge.h"

// For the breakout, you can use any 2 or 3 pins
// These pins will also work for the 1.8" TFT shield
// Data signal
#define TFT_CS1    10
#define TFT_CS2    7
#define TFT_CS3    8
#define TFT_CS4    6
#define TFT_CS5    5
#define TFT_CS6    4

// Reset signal
/*#define TFT_RST1   -1
#define TFT_RST2   8
#define TFT_RST3   2
#define TFT_RST4   A4
#define TFT_RST5   A5
#define TFT_RST6   A2*/

#define TFT_RST1   -1
#define TFT_RST2   -1
#define TFT_RST3   -1
#define TFT_RST4   -1
#define TFT_RST5   -1
#define TFT_RST6   -1

#define TFT_DC     9 // Common to all screens


#define BLACK 0x0000
#define BLUE 0x001F
#define RED 0xF800
#define GREEN 0x07E0
#define CYAN 0x07FF
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define WHITE 0xFFFF

Adafruit_ST7735 tft1 = Adafruit_ST7735(TFT_CS1, TFT_DC, TFT_RST1);
Adafruit_ST7735 tft2 = Adafruit_ST7735(TFT_CS2, TFT_DC, TFT_RST2);
Adafruit_ST7735 tft3 = Adafruit_ST7735(TFT_CS3, TFT_DC, TFT_RST3);
Adafruit_ST7735 tft4 = Adafruit_ST7735(TFT_CS4, TFT_DC, TFT_RST4);
Adafruit_ST7735 tft5 = Adafruit_ST7735(TFT_CS5, TFT_DC, TFT_RST5);
Adafruit_ST7735 tft6 = Adafruit_ST7735(TFT_CS6, TFT_DC, TFT_RST6);

void setup() {
	tft1.initR(INITR_BLACKTAB);
  tft2.initR(INITR_BLACKTAB);      // Init ST7735S chip, black tab
	tft3.initR(INITR_BLACKTAB);
 	tft4.initR(INITR_BLACKTAB);
	tft5.initR(INITR_BLACKTAB);
 	tft6.initR(INITR_BLACKTAB);

  //tft1.setRotation(random(4)); 
  //tft2.setRotation(random(4));
  //tft3.setRotation(random(4));
  //tft4.setRotation(random(4));
  //tft5.setRotation(random(4));
  //tft6.setRotation(random(4));

	tft1.fillScreen(ST7735_BLACK);
  tft2.fillScreen(ST77XX_BLACK);
  tft3.fillScreen(ST77XX_BLACK);
  tft4.fillScreen(ST77XX_BLACK);
  tft5.fillScreen(ST77XX_BLACK);
  tft6.fillScreen(ST77XX_BLACK);

  int h = 128,w = 160, row, col, buffidx=0;
  tft1.drawBitmap(0, 0, bitmap_allArray[0], h, w, WHITE);
  tft2.drawBitmap(0, 0, bitmap_allArray[1], h, w, WHITE);
  tft3.drawBitmap(0, 0, bitmap_allArray[2], h, w, WHITE);
  tft4.drawBitmap(0, 0, bitmap_allArray[3], h, w, WHITE);
  tft5.drawBitmap(0, 0, bitmap_allArray[4], h, w, WHITE);
  tft6.drawBitmap(0, 0, bitmap_allArray[5], h, w, WHITE);
}

void ShuffeledIndexes(int* indexes, int arraySize = 6)
{
  for (int i = 5; i > 0; i--){
    int j = random(i + 1);

    int temp = indexes[i];
    indexes[i] = indexes[j];
    indexes[j] = temp;
  }
}

void loop() {
  // Prepare the list of indexes
  int imgIndexes[6];
  for (int i = 0; i < 6; i++){
    imgIndexes[i] = i;
  }

  ShuffeledIndexes(imgIndexes);

  	tft1.fillScreen(ST7735_BLACK);
  tft2.fillScreen(ST77XX_BLACK);
  tft3.fillScreen(ST77XX_BLACK);
  tft4.fillScreen(ST77XX_BLACK);
  tft5.fillScreen(ST77XX_BLACK);
  tft6.fillScreen(ST77XX_BLACK);

  int h = 128,w = 160, row, col, buffidx=0;
  tft1.drawBitmap(0, 0, bitmap_allArray[imgIndexes[0]], h, w, WHITE);
  tft2.drawBitmap(0, 0, bitmap_allArray[imgIndexes[1]], h, w, WHITE);
  tft3.drawBitmap(0, 0, bitmap_allArray[imgIndexes[2]], h, w, WHITE);
  tft4.drawBitmap(0, 0, bitmap_allArray[imgIndexes[3]], h, w, WHITE);
  tft5.drawBitmap(0, 0, bitmap_allArray[imgIndexes[4]], h, w, WHITE);
  tft6.drawBitmap(0, 0, bitmap_allArray[imgIndexes[5]], h, w, WHITE);

  delay(2000);
}
