/*
  Arduino LSM6DS3 - Simple Accelerometer

  This example reads the acceleration values from the LSM6DS3
  sensor and continuously prints them to the Serial Monitor
  or Serial Plotter.

  The circuit:
  - Arduino Uno WiFi Rev 2 or Arduino Nano 33 IoT

  created 10 Jul 2019
  by Riccardo Rizzo

  This example code is in the public domain.
*/

#include <Arduino_LSM6DS3.h>

float x, y, z;
int degreesX = 0;
int degreesY = 0;

void setup() {
  Serial.begin(9600);
  while (!Serial);

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");

    while (1);
  }

  Serial.print("Accelerometer sample rate = ");
  Serial.print(IMU.accelerationSampleRate());
  Serial.println(" Hz");

}

void loop() {
  float x, y, z;

  if (IMU.accelerationAvailable()) {
    IMU.readAcceleration(x, y, z);
    
    if(x > 0.1){
      x = 100*x;
      degreesX = map(x, 0, 97, 0, 90);
      Serial.print("Tilting DOWN ");
      Serial.print(degreesX);
      Serial.println("  degrees");
      }
    if(x < -0.1){
      x = 100*x;
      degreesX = map(x, 0, -100, 0, 90);
      Serial.print("Tilting UP ");
      Serial.print(degreesX);
      Serial.println("  degrees");
      }
    if(y > 0.1){
      y = 100*y;
      degreesY = map(y, 0, 97, 0, 90);
      Serial.print("Tilting RIGHT ");
      Serial.print(degreesY);
      Serial.println("  degrees");
      }
    if(y < -0.1){
      y = 100*y;
      degreesY = map(y, 0, -100, 0, 90);
      Serial.print("Tilting LEFT ");
      Serial.print(degreesY);
      Serial.println("  degrees");
      }
      delay(1000);
    
}
}
