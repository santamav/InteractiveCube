#include "CubeIMU.h"

CubeIMU::CubeIMU()
{
    // Initialize the IMU
    if (!IMU.begin()) {
        Serial.println("Failed to initialize IMU!");
        while (1);
    }

    setup();
}

void CubeIMU::setup()
{
    // Sett initial values
    resetRotation();
    previousMillis = 0;
}

void CubeIMU::updateRotation()
{
    long currentMillis = millis();
    // Read the gyroscope values
    float gx, gy, gz; // Gyroscope values
    if (IMU.gyroscopeAvailable()) {
        IMU.readGyroscope(gx, gy, gz);
    }

    // Compute the rotation in degrees since last update
    x += gx * (currentMillis - previousMillis) / 1000;
    y += gy * (currentMillis - previousMillis) / 1000;
    z += gz * (currentMillis - previousMillis) / 1000;

    //Keep  the rotation values between 0 and 360?
    x = fmod(x, 360);
    y = fmod(y, 360);
    z = fmod(z, 360);

    // Update the previous time
    previousMillis = currentMillis;
}

void CubeIMU::resetRotation()
{
    x, y, z = 0;
}

float* CubeIMU::getRotation()
{
    return new float[3]{x, y, z};
}