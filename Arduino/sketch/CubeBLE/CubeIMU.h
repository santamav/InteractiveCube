#include <Arduino_LSM6DS3.h> // Imu library

class CubeIMU
{
    public:
        CubeIMU();
        void setup();
        void updateRotation();
        void resetRotation();
        float* getRotation();
    private:
        long previousMillis; // Current time in ms
        float x, y, z; // Current rotation values
};