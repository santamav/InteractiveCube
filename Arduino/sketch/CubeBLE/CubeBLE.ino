#include <MadgwickAHRS.h>
#include <ArduinoBLE.h> // BLE library
#include <Arduino_LSM6DS3.h> // Imu library
#include <Adafruit_ST7735.h>
#include <Adafruit_GFX.h>

#include "bitmaps.h"



//TODO: Would be nice to have a class/screen to handle the TFT screens
// Define pins for the TFT screens
#define TFT_CS1    10
#define TFT_CS2    7
#define TFT_CS3    8
#define TFT_CS4    6
#define TFT_CS5    5
#define TFT_CS6    4

//Reset signal
#define TFT_RST1   -1
#define TFT_RST2   -1
#define TFT_RST3   -1
#define TFT_RST4   -1
#define TFT_RST5   -1
#define TFT_RST6   -1

#define TFT_DC     9 // Common to all screens

// Define the screens
Adafruit_ST7735 tft1 = Adafruit_ST7735(TFT_CS1, TFT_DC, TFT_RST1);
Adafruit_ST7735 tft2 = Adafruit_ST7735(TFT_CS2, TFT_DC, TFT_RST2);
Adafruit_ST7735 tft3 = Adafruit_ST7735(TFT_CS3, TFT_DC, TFT_RST3);
Adafruit_ST7735 tft4 = Adafruit_ST7735(TFT_CS4, TFT_DC, TFT_RST4);
Adafruit_ST7735 tft5 = Adafruit_ST7735(TFT_CS5, TFT_DC, TFT_RST5);
Adafruit_ST7735 tft6 = Adafruit_ST7735(TFT_CS6, TFT_DC, TFT_RST6);


// BLE Services and characteristics
BLEService imuService ("451292fc-e080-11ed-b5ea-0242ac120002");
BLECharacteristic rotCharacteristic("83defa8a-e07f-11ed-b5ea-0242ac120002", BLERead | BLEWrite | BLENotify, 3 * sizeof(float)); // Gyroscope characteristic x, y, z values
BLECharacteristic accCharacteristic("83deff8a-e07f-11ed-b5ea-0242ac120002", BLERead | BLEWrite | BLENotify, 3 * sizeof(float));

BLEService displayService ("fb0beb70-d76f-4fbf-9a8b-b522bcb965f5");
BLECharacteristic displayImageCharacteristic("9abd41fc-5415-480c-8610-48969adf255b", BLERead | BLEWrite, 6 * sizeof(int)); // Display image characteristic
BLECharacteristic displayOrientationCharacteristic("9abd4afc-5415-480c-8610-48969adf255b", BLERead | BLEWrite, 6 * sizeof(int)); // Display position and rotation pairs characteristic

// BLE properties
BLEDevice centralDevice; // Central device
bool connected = false; // Connection status

 // Display properties
int* previousDisplayImages = new int[6]{0, 0, 0, 0, 0, 0};
int* previousDisplayRotations = new int[6]{3, 4, 2, 1, 4, 3};

// IMU properties
float currentYaw = 0, currentPitch = 0, currentRoll = 0; // Current rotation values
Madgwick filter; // Filter for the IMU
const float sensorRate = 104.00; // Sensor rate in Hz
const float sendRotRate = 18; // Send rotation rate in Hz
bool rotationRequested = false; // Send rotation data flag

float last_ble = 0; // Last time the data was sent
float last_rot_update = 0;

void setup()
{
    Serial.begin(9600);    // initialize serial communication
    pinMode(LED_BUILTIN, OUTPUT); // initialize the built-in LED pin to indicate when a central is connected

    //Initialize Screens
    tft1.initR(INITR_BLACKTAB);
    tft2.initR(INITR_BLACKTAB);      // Init ST7735S chip, black tab
    tft3.initR(INITR_BLACKTAB);
    tft4.initR(INITR_BLACKTAB);
    tft5.initR(INITR_BLACKTAB);
    tft6.initR(INITR_BLACKTAB);

    fillDisplaWithColor(ST77XX_RED);

    // Initialize BLE
    if (!BLE.begin()) {
        Serial.println("starting BLE failed!");
        while (1);
    }

    // Initialize the IMU
    if (!IMU.begin()) {
        Serial.println("Failed to initialize IMU!");
        while (1);
    }

    filter.begin(sensorRate);

    // Set a local name for the BLE device
    BLE.setLocalName("CubePoseBLE001");
    BLE.setAdvertisedService(imuService);
    
    // Add the service and characteristic
    imuService.addCharacteristic(rotCharacteristic);
    imuService.addCharacteristic(accCharacteristic);
    BLE.addService(imuService);

    displayService.addCharacteristic(displayImageCharacteristic);
    displayService.addCharacteristic(displayOrientationCharacteristic);
    BLE.addService(displayService);

    BLE.setEventHandler(BLEConnected, [](BLEDevice central) {
        Serial.print("Connected to central: ");
        // print the central's BT address:
        Serial.println(central.address());
        // turn on the LED to indicate the connection:
        digitalWrite(LED_BUILTIN, HIGH);
        fillDisplaWithColor(ST77XX_GREEN);
    });

    BLE.setEventHandler(BLEDisconnected, [](BLEDevice central) {
        Serial.print("Disconnected from central: ");
        // print the central's BT address:
        Serial.println(central.address());
        // turn off the LED to indicate the connection:
        digitalWrite(LED_BUILTIN, LOW);
        fillDisplaWithColor(ST77XX_RED);
    });

    // Set initial value for the characteristic
    sendRotationData(new float[3]{0, 0, 0});

    // Set characteriistics event listeners
    rotCharacteristic.setEventHandler(BLEWritten, onRotationRequested);
    accCharacteristic.setEventHandler(BLEWritten, onAccDataRequest);
    displayImageCharacteristic.setEventHandler(BLEWritten, onDisplayImageWriten);
    displayOrientationCharacteristic.setEventHandler(BLEWritten, onOrientationsWritten);
    
    // Start advertising
    BLE.advertise();

    setDisplayOrientations(previousDisplayRotations);
    
    Serial.println("CubeBLE peripheral active, waiting for connections...");
}


void loop()
{
  // wait for a Bluetooth® Low Energy central
    BLEDevice central = BLE.central();
    // if a central is connected to the peripheral:
    if (central) {
        // while the central is connected:
        while (central.connected()) { //TODO: This while loop is blocking, should be changed to a non-blocking loop
            float time_since_last_rot = millis() - last_rot_update;
            float time_since_last = millis() - last_ble;
            // Update the gyroscope values
            if(time_since_last_rot > 1000/sensorRate)
            {
                updateRotation();
                last_rot_update = millis();
            }
            // Retrieve BLE events
            BLE.poll(); // Can specify a timeout for waiting for a new event, default is 0ms
            // Send the rotation data
            if(rotationRequested && time_since_last > 1000/sendRotRate) // Send data every 200ms
            {
                sendRotationData(readRotationData());
                last_ble = millis();
            }
        }
    }
}


// ************************ EVENT HANDLERS ************************
/// @brief On rotation data requested event
void onRotationRequested(BLEDevice central, BLECharacteristic characteristic)
{
    rotationRequested = !rotationRequested;
    Serial.print("Rotation requested: ");
}

/// @brief On accelerometer data request event
void onAccDataRequest(BLEDevice device, BLECharacteristic characteristic)
{
    // No need to read the value, we only need the event
    sendAccData(readAccData());
}

/// @brief On display image BLE characteristic written event
/// @param central central device
/// @param characteristic characteristic written
void onDisplayImageWriten(BLEDevice central, BLECharacteristic characteristic)
{
    // Read the data from the characteristic
    byte data[6 * sizeof(int)];
    characteristic.readValue(data, 6 * sizeof(int));

    // Transform the data to an int array
    int displayData[6];
    memcpy(displayData, data, 6 * sizeof(int));

    // Set the images in the 
    setDisplayImages(displayData);
}

/// @brief On display orientations BLE characteristic written event
/// @param central Central device
/// @param characteristic Characteristic written
void onOrientationsWritten(BLEDevice central, BLECharacteristic characteristic)
{
    // Read the data from the characteristic
    byte data[6 * sizeof(int)];
   characteristic.readValue(data, 6 * sizeof(int));

    // Transform the data to an int array
    int rot[6];
    memcpy(rot, data, 6 * sizeof(int));

    // Set the display positions and orientations
    setDisplayOrientations(rot);
    // Show the images in the new orientations
    setDisplayImages(); 
}
// *****************************************************************

/// @brief Get the last computed rotation
/// @return yaw, pitch and roll values
float* readRotationData()
{
    return new float[3]{currentPitch, currentRoll, currentYaw};
}

/// @brief Write the gyroscope data into the BLE Characteristic
/// @param gyroData float with the x, y z values of the current cube rotation //TODO: It is not the gyroscope data but the current cibe rotation
void sendRotationData(float* gyroData)
{
    Serial.print("Sending rotation data: ");
    Serial.print(gyroData[0]);
    Serial.print(" ");
    Serial.print(gyroData[1]);
    Serial.print(" ");
    Serial.println(gyroData[2]);
    // Transform data to byte array
    uint8_t byteData[3 * sizeof(float)];
    memcpy(byteData, gyroData, 3 * sizeof(float));

    // Write the byte array to the BLE characteristic
    rotCharacteristic.setValue(byteData, 3 * sizeof(float));
}

/// @brief Read the accelerometer data
/// @return A float of size 3 with the x, y, z values of the accelerometer
float* readAccData()
{
    float ax=0,ay=0,az=0;
    if(IMU.accelerationAvailable())
        IMU.readAcceleration(ax, ay, az);

    return new float[3] {ax, ay, az};
}

/// @brief Write the accelerometer values into the BLE characteristic
/// @param accValues 
void sendAccData(float* accValues)
{
    //TODO: Should make a function to generalize this
        // void SendData(uint8_t* data, int sizeOfData, BLECharacteristic characteristic)
        // uint8_t* vector3ToByteArray(x, y, z)
    // Transform the data to byte array
    uint8_t byteData[3 * sizeof(float)];
    memcpy(byteData, accValues, 3*sizeof(float));

    //Write bvalues into the BLE Characteristic
    accCharacteristic.setValue(byteData, 3*sizeof(float));
}

/// @brief Update the rotation values of the cube
void updateRotation()
{
  // values for acceleration & rotation:
  float xAcc, yAcc, zAcc;
  float xGyro, yGyro, zGyro;
  
  // values for orientation:
  float roll, pitch, heading;
  // check if the IMU is ready to read:
  if (IMU.accelerationAvailable() &&
      IMU.gyroscopeAvailable()) {
    // read accelerometer & gyrometer:
    IMU.readAcceleration(xAcc, yAcc, zAcc);
    IMU.readGyroscope(xGyro, yGyro, zGyro);

    // update the filter, which computes orientation:
    filter.updateIMU(xGyro, yGyro, zGyro, xAcc, yAcc, zAcc);

    // print the heading, pitch and roll
    roll = filter.getRoll();
    pitch = filter.getPitch();
    heading = filter.getYaw();

    currentRoll = roll;
    currentPitch = pitch;
    currentYaw = heading;
    
    // Serial.print("Orientation: ");
    // Serial.print(heading);
    // Serial.print(" ");
    // Serial.print(pitch);
    // Serial.print(" ");
    // Serial.println(roll);
  }
}


/// @brief Set default or stored images in the displays
void setDisplayImages()
{
    setDisplayImages(previousDisplayImages);
}

/// @brief Set the images to be displayed in the screens
/// @param icons list of indexes of the images
void setDisplayImages(int* icons)
{
    setDisplayImages(icons, ST77XX_WHITE);
}

/// @brief Set the images to be displayed in the screens
/// @param icons list of indexes of the images
/// @param color tint color of the mages
void setDisplayImages(int* icons, uint16_t color)
{
    fillDisplaWithColor(ST77XX_BLACK);

    int h = 128,w = 128, row, col, buffidx=0;
    drawCenterdBitmap(tft1, icons[0], 128, 128, color);
    drawCenterdBitmap(tft2, icons[1], 128, 128, color);
    drawCenterdBitmap(tft3, icons[2], 128, 128, color);
    drawCenterdBitmap(tft4, icons[3], 128, 128, color);
    drawCenterdBitmap(tft5, icons[4], 128, 128, color);
    drawCenterdBitmap(tft6, icons[5], 128, 128, color);

    memccpy(previousDisplayImages, icons, 6 * sizeof(int), 6 * sizeof(int));
}

/// @brief Draw a bitmap in the center of the screen
/// @param tft screeen reference
/// @param index bitmap index in the images array
/// @param w witdh of the image
/// @param h height of the display
/// @param color Color tint
void drawCenterdBitmap(Adafruit_ST7735 tft, int index, int w, int h, uint16_t color)
{
    // Dsiplay dimensions
    int dw, dh; // Display width and height
    dw = tft.width();
    dh = tft.height();
    // Compute the position to center the image
    int row = (dw - w) / 2, col = (dh - h) / 2, buffidx=0;
    tft.drawBitmap(row, col, bitmap_allArray[index], w, h, color);
}

/// @brief Fill all displays with a color
/// @param color Color to fill
void fillDisplaWithColor(uint16_t color)
{
    tft1.fillScreen(color);
    tft2.fillScreen(color);
    tft3.fillScreen(color);
    tft4.fillScreen(color);
    tft5.fillScreen(color);
    tft6.fillScreen(color);
}

/// @brief Set the display orientations
/// @param rot Array with the display orientations
void setDisplayOrientations(int* rot)
{
    Serial.print("Setting display orientations: ");
    Serial.print(rot[0]);
    Serial.print(" ");
    Serial.print(rot[1]);
    Serial.print(" ");
    Serial.print(rot[2]);
    Serial.print(" ");
    Serial.print(rot[3]);
    Serial.print(" ");
    Serial.print(rot[4]);
    Serial.print(" ");
    Serial.println(rot[5]);

    tft1.setRotation(rot[0]);
    tft2.setRotation(rot[1]);
    tft3.setRotation(rot[2]);
    tft4.setRotation(rot[3]);
    tft5.setRotation(rot[4]);
    tft6.setRotation(rot[5]);

    memccpy(previousDisplayRotations, rot, 6 * sizeof(int), 6 * sizeof(int));
}