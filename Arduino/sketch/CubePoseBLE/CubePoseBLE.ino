

#include <ArduinoBLE.h>
#include <Arduino_LSM6DS3.h>


enum CONTROL_ORDER {
  NONE,
  SEND_GYRO,
  READ_DISPLAY_ICONS
};

// We can combine values in one characteristic https://www.arduino.cc/reference/en/libraries/arduinoble/
 // Bluetooth® Low Energy Gyroscope service
BLEService cubeService ("451292fc-e080-11ed-b5ea-0242ac120002");
// Bluetooth® Low Energy Gyroscope x, y and z characteristics
BLEFloatCharacteristic XReading("83defa8a-e07f-11ed-b5ea-0242ac120002",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes
BLEFloatCharacteristic YReading("83deff8a-e07f-11ed-b5ea-0242ac120002",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes
BLEFloatCharacteristic ZReading("83df03fe-e07f-11ed-b5ea-0242ac120002",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes


// Bluetooth® Low Energy control service
BLEService controlService("fb0beb70-d76f-4fbf-9a8b-b522bcb965f5");
// Control characteristic -> GameState or orders
BLEIntCharacteristic controlReading("9abd41fc-5415-480c-8610-48969adf255b",
    BLERead | BLEWrite); // Control order received from the game
BLECharacteristic controlData("9abd4afc-5415-480c-8610-48969adf255b",
    BLERead | BLEWrite, 6 * sizeof(int)); // The data has a max syze of 20 bytes

long previousMillis = 0;  // last time the battery level was checked, in ms
int32_t currentOrder = 0;

void setup() {
  Serial.begin(9600);    // initialize serial communication
  //while (!Serial);

  pinMode(LED_BUILTIN, OUTPUT); // initialize the built-in LED pin to indicate when a central is connected

  // begin initialization
  if (!BLE.begin()) {
    Serial.println("starting BLE failed!");

    while (1);
  }

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");
    while (1);
  }


  /* Set a local name for the Bluetooth® Low Energy device
     This name will appear in advertising packets
     and can be used by remote devices to identify this Bluetooth® Low Energy device
     The name can be changed but maybe be truncated based on space left in advertisement packet
  */
  BLE.setLocalName("CubePoseBLE001");
  BLE.setAdvertisedService(cubeService); // advertise the gyro data service

  cubeService.addCharacteristic(XReading); // add XAxis characteristic
  cubeService.addCharacteristic(YReading); // add YAxis characteristic
  cubeService.addCharacteristic(ZReading); // add ZAxis characteristic
  BLE.addService(cubeService); // Add the gyroscope data service

  // Add characteristcs to the service
  controlService.addCharacteristic(controlReading);
  controlService.addCharacteristic(controlData);
  BLE.addService(controlService); // Add the control service
  // Set default values
  XReading.writeValue(0); // set initial value for this characteristic
  YReading.writeValue(0); // set initial value for this characteristic
  ZReading.writeValue(0); // set initial value for this characteristic
  // Set default values
  controlReading.writeValue(0);
  
  // Setup on written events
  controlReading.setEventHandler(BLEWritten, onCntrlCharacteristicWritten);
  controlData.setEventHandler(BLEWritten, onDataCharacteristicWritten);

  /* Start advertising Bluetooth® Low Energy.  It will start continuously transmitting Bluetooth® Low Energy
     advertising packets and will be visible to remote Bluetooth® Low Energy central devices
     until it receives a new connection */

  // start advertising
  BLE.advertise();

  Serial.println("Bluetooth® device active, waiting for connections...");
}

void loop() {
  // wait for a Bluetooth® Low Energy central
  BLEDevice central = BLE.central();

  // if a central is connected to the peripheral:
  if (central) {
    Serial.print("Connected to central: ");
    // print the central's BT address:
    Serial.println(central.address());
    // turn on the LED to indicate the connection:
    digitalWrite(LED_BUILTIN, HIGH);

    // while the central is connected:
    while (central.connected()) {
      BLE.poll(); // Can specify a timeout for waiting for a new event, default is 0ms

      /*if(currentOrder == 1)
      {
        send_gyro_data();
      }*/
    }
  }
  // when the central disconnects, turn off the LED:
  digitalWrite(LED_BUILTIN, LOW);
}

// Callback function when the characteristic is written
void onCntrlCharacteristicWritten(BLEDevice central, BLECharacteristic characteristic) {
  // Ensure the characteristic is the one you're interested in
  if (characteristic.uuid() == controlReading.uuid()) {
    // Read the new value of the characteristic
    int32_t newValue;
    controlReading.readValue(newValue);

    // Do something with the new value
    Serial.print("Characteristic value written: ");
    Serial.println(newValue);

    if(newValue != currentOrder)
      currentOrder = newValue;
  }
}

void onDataCharacteristicWritten(BLEDevice central, BLECharacteristic characteristic)
{
  if(characteristic.uuid() != controlData.uuid()) return;

  currentOrder = READ_DISPLAY_ICONS;
  // Get the size of the data
  int dataLength = controlData.valueLength();
  handle_data_received(currentOrder, dataLength);
}

// Read the received data and based on the las received order do things
void handle_data_received(int32_t order, int dataLength)
{
  switch(order)
  {
    case SEND_GYRO:
      read_gyro_cntrl_data(dataLength);
      break;
    case READ_DISPLAY_ICONS:
      // Should check first if the size of the data is valid
      read_displays_cntrl_data(dataLength);
      break;
  }
}

void read_displays_cntrl_data(int dataLength)
{
  // Displays data should be an int array of size 6
  Serial.println("Reading display icons");

  byte data[dataLength*sizeof(int)];
  controlData.readValue(data, dataLength);

  int displayData[6];
  memcpy(displayData, data, dataLength);

  for(int i = 0; i < 6; i++)
  {
    Serial.print(displayData[i]);
    Serial.print('\t');
  }

}

void read_gyro_cntrl_data(int dataLength)
{
  Serial.println("Preparing to send Gyro data");
  // No need to do anything else 
}

/// Manage reading the gyroscope and sending it through BLE
void send_gyro_data()
{
  long currentMillis = millis();
  // if 200ms have passed, check the battery level:
  float dTime = currentMillis - previousMillis; 
  float gX, gY, gZ, aX, aY, aZ;

  if (IMU.gyroscopeAvailable()) {
    IMU.readGyroscope(gX, gY, gZ);
  }

  if (IMU.accelerationAvailable()){
    IMU.readAcceleration(aX, aY, aZ);
  }

  float x, y, z;
  x = gX * dTime / 1000;
  y = gY * dTime / 1000;
  z = gZ * dTime / 1000;


  //Serial.print(x);
  XReading.writeValue(x);  // and update the battery level characteristic
  //Serial.print('\t');
  //Serial.print(y);
  YReading.writeValue(y); 
  //Serial.print('\t');
  //Serial.println(z);
  ZReading.writeValue(z); 

  previousMillis = currentMillis;    
}